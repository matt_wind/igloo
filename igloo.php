<?php
  /**
  * Igloo.php is used to calculate the smallest possible square area of snow that can be used to 
  * build an igloo of a given diameter.
  *
  * First line gets a diameter from the user and checks if it is a number or not.
  */

  do {
    $diameter = readline("Please enter the diameter of the igloo (as a number): ");
  } while (!is_numeric($diameter));

  /**
  * Math here first gets area of a full sphere with given diameter
  * Then divides it by two to get the area of the half sphere
  * Then multiplies it by 1.12 to account for the 12 percent snow loss in the cutting process
  * And lastly roots it to get the length and the width, as the thickness does not matter
  * in this case.
  */

  $area = 4 * 3.14 * ($diameter / 2);
  $area = $area / 2;
  $area = $area * 1.12;
  $length = sqrt($area);

  /**
  * Since length and width are the same you only need to print length
  */

  print "length and width of the smallest square area of packed snow is: $length \n";
?>
